<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('/', function () {
//     return view('includes.dashboard');
// });
// Route::get('/', 'TicketController@index')->name('home');
Route::post('/events', 'TicketController@store')->name('store');
Route::get('/events', function(){
    return view('events');
});
Route::Resource('tickets', 'TicketController');

Route::get('/events', 'TicketController@index')->name('events');
Route::get('/edit/{id}', 'TicketController@edit')->name('edit');
Route::post('/update/{id}', 'TicketController@update')->name('update');
Route::delete('/delete/{id}', 'TicketController@delete')->name('delete');

Auth::routes();

Route::get('/', 'TicketController@login')->name('home');
