<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tickets extends Model
{
    //

    protected $fillable=['eventname','eventdate','description','image','ticket','price','attendees'];
    protected $primaryKey='id';
    
}
