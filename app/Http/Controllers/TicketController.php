<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tickets;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // get all data from ticket model and pass to welcome using the compact();
        $tickets= Tickets::paginate(5);
        return View('events', compact('tickets'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function login(){
        return view('includes.dashboard');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'eventname'=>'bail|required',
            'eventdate'=>'bail|required',
            'description'=>'bail|required',
            'ticket'=>'bail|required',
            'image'=>'nullable',
            'price'=>'bail|required',
            'attendees'=>'bail|required'
            ]);
            //define the model

            $tickets=new Tickets;
            $tickets->eventname= $request->eventname;
            $tickets->eventdate= $request->eventdate;
            $tickets->description= $request->description;
            $tickets->ticket= $request->ticket;
            $tickets->image= $request->image;
            $tickets->price= $request->price;
            $tickets->attendees=$request->attendees;
            $tickets->save();

        //redirect back home with a success message
        return redirect(route('events'))->with('success',"Ticket added successfully");

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $tickets=Tickets::find($id);
        return view('edit', compact('tickets'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'eventname'=>'required',
            'eventdate'=>'required',
            'description'=>'required',
            'tickettype'=>'required',
            'price'=>'required',
            'attendees'=>'required'
        ]);

        // define model
        // define model
        $ticket=new Tickets;
        $ticket->eventname= $request->eventname;
        $ticket->eventdate= $request->eventdate;
        $ticket->description= $request->description;
        $ticket->ticket= $request->tickettype;
        $tickets->image= $request->image;
        $ticket->price= $request->price;
        $ticket->attendees=$request->attendees;
        $ticket->save();
        //redirect back home with a success message
        return redirect(route('edit'))->with('success','Ticket updated.
         successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        //
        Tickets::find($id)->delete();
        return redirect(route('home'))->with('success',"data deleted");
    }
}
