@extends('layout.main')

@section('content')
<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">First</th>
        <th scope="col">Last</th>
        <th scope="col">Handle</th>
      </tr>
    </thead>
    <tbody>
     @foreach ($tickets as $item)
     <tr>
        <th scope="row">1</th>
        <td>{{$item->eventname}}</td>
     <td>{{ $item->eventdate}}</td>
     <td>{{ $item->ticket}}</td>
      </tr>
         
     @endforeach
     
    </tbody>
  </table>
    
@endsection