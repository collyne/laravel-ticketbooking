@extends('layout.main')
@section('content')
<div class="card mb-5">
        <div class="card-header bg-dark text-white text-center">
            <i class="fa fa-dashboard fa-2x pull-left"></i>
            Main dashboard
        </div>
        <div class="card-body">
            <div class="row">
        
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-header bg-dark text-white text-center">
                            Events
                        </div>
                        <div class="card-body">
                                <i class="fa fa-microphone fa-5x"></i>
                                <div class="pull-right">
                                    <h3>3</h3>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-header bg-dark text-white text-center">
                            Registered Attendees
                        </div>
                        <div class="card-body">
                            <i class="fa fa-group fa-5x"></i>
                            <div class="pull-right">
                                    <h3>0</h3>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-header bg-dark text-white text-center">
                           Booked tickets
                        </div>
                        <div class="card-body">
                            <i class="fa fa-book fa-5x"></i>
                            <div class="pull-right">
                                    <h3>0</h3>
                                </div>
                        </div>
                    </div>
                </div>
              
                    
            </div>
    
        </div>
        <div class="card-footer bg-dark">
    
        </div>
    </div>
        
@endsection
