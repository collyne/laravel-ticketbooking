@extends('layout.main')

@section('content')
<div class="container">
        @if(session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif
    <div class="card">
        <div class="card-header text-white text-center bg-dark">
            <a href="/events" title="back" class="btn btn-outline-danger btn-sm pull-left"><i class="fa fa-arrow-left"></i></a>
            Update data
        </div>
        <div class="card-body">
            <form action="{{ route('update', $tickets->id)}}" method="POST" autocomplete="off">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Event name:</label>
                        <input type="text" class="form-control" value="{{ $tickets->eventname}}" name="eventname">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Event date:</label>
                            <input type="date" class="form-control" value="{{ $tickets->eventdate}}" name="eventdate">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Event Description:</label>
                            <textarea class="form-control" name="description" cols="10" rows="5">{{ $tickets->description}}"</textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                            <div class="form-group">
                                <label>Featured Message:</label>
                            <input type="text" value="{{ $tickets->image}}" class="form-control" name="image">
                            </div>
                        </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Ticket Type:</label>
                            <select class="form-control" name="ticket">
                                <option>{{ $tickets->ticket}}</option>
                                <option>Regular</option>
                                <option>V.I.P</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Ticket Price:</label>
                            <input type="number" value="{{ $tickets->price}}" class="form-control" name="price">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Attendees(Total):</label>
                            <input type="number" value="{{ $tickets->attendees}}" class="form-control" name="attendees">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input class="btn btn-dark btn-sm" type="submit" value="Update">
                    </div>
                </div>      
            </form>
        </div>
    </div>
</div>
    
@endsection
