@extends('layout.main')
@section('content')

<div class="card-body">
        @if($errors->any())
        @foreach ($errors->all() as $error)
        
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    {{ $error }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
            
        @endforeach

    @endif
        @if(session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header bg-dark text-white text-center">
                        <i class="fa fa-bell fa-2x pull-left"></i>
                        Events
                        <a href="#" class="btn btn-outline-danger btn-sm pull-right" data-toggle="modal" data-target="#addTicket">
                            <i class="fa fa-plus"></i>
                        </a>
                    </div>
                    <div class="card-body">
                        <table class="table table-responsive table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Event name</th>
                                    <th>Event date</th>
                                    <th>Description</th>
                                    <th>Attendees</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($tickets as $key=>$ticket)
                                   <tr>
                                   <td scope="row">{{ ++$key}}</td>
                                   <td>{{ $ticket->eventname }}</td>
                                   <td>{{ $ticket->eventdate}}</td>
                                   <td width="50%">{{ $ticket->description}}</td>
                                   <td>{{ $ticket->attendees}}</td>
                                  <td>
                                  <a href="{{ route('edit', $ticket->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                      <a href="" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></a>

                                    <form method="post"  id="delete-id-{{$ticket->id}}" action="{{ route('delete', $ticket->id)}}" style="display:none">
                                    @csrf
                                    {{method_field('delete')}}
                                    </form>
                                    <button onclick="if(confirm('Are you sure you wanna delete?')){
                                    event.preventDefault();
                                    document.getElementById('delete-id-{{$ticket->id}}').submit();
                                    }else{
                                    event.preventDefault();
                                    }"
                                    href="{{ route('edit', $ticket->id)}}" class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i>
                                    </button>                                  </td>
                              </tr>
                                   @endforeach
                                  
                             
                            </tbody>
                        </table>  
                        {{$tickets->links()}}  
                    </div>
                </div>
            </div>
        </div>
        <div>
            {{--======================= modal form for ticket registration=============================== --}}
        <div class="modal fade top" id="addTicket" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header bg-dark text-white">
                      <h5 class="modal-title" id="exampleModalLabel">Add Ticket</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                         <div class="container">
                                <div class="row">
                                <form action="{{ route('store')}}" method="POST" autocomplete="off">
                                    @csrf
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Event name:</label>
                                                        <input type="text" class="form-control" name="eventname">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Event date:</label>
                                                        <input type="date" class="form-control" name="eventdate">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Event Description:</label>
                                                        <textarea class="form-control" name="description" cols="10" rows="5"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Featured Message:</label>
                                                            <input type="text" class="form-control" name="image">
                                                        </div>
                                                    </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Ticket Type:</label>
                                                        <select class="form-control" name="ticket">
                                                            <option>--select-ticket--</option>
                                                            <option>Regular</option>
                                                            <option>V.I.P</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Ticket Price:</label>
                                                        <input type="number" class="form-control" name="price">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Attendees(Total):</label>
                                                        <input type="number" class="form-control" name="attendees">
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <input class="btn btn-dark btn-sm" type="submit" value="Submit data">
                                                </div>
                                            </div>      
                                        </form>
                                    </div>
                                </div>
                         </div>
                  </div>
                </div>
              </div>
    
@endsection('content')
